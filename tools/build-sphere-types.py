#!/usr/bin/env python

methods = {
    "Workspace": [
        "ActivateUser",
        "FreezeUser",
        "InitUser",
        "GetUsers",
        "GetUser",
        "UpdateUser",
        "GetUserPublicKeys",
        "AddUserPublicKey",
        "DeleteUserPublicKey",
        "DeleteUserPublicKeys",
        "GetProjects",
        "GetProject",
        "CreateProject",
        "UpdateProject",
        "DeleteProject",
        "GetProjectMembers",
        "GetProjectMember",
        "AddProjectMember",
        "UpdateProjectMember",
        "DeleteProjectMember",
        "ActivateOrganization",
        "FreezeOrganization",
        "GetOrganizations",
        "GetOrganization",
        "CreateOrganization",
        "UpdateOrganization",
        "DeleteOrganization",
        "GetOrganizationMembers",
        "GetOrganizationMember",
        "RequestOrganizationMembership",
        "ConfirmOrganizationMembership",
        "UpdateOrganizationMember",
        "DeleteOrganizationMember",
        "GetOrganizationProjects",
        "GetOrganizationProject",
        "AddOrganizationProject",
        "UpdateOrganizationProject",
        "DeleteOrganizationProject",
        "GetExperiments",
        "GetProjectExperiments",
        "GetExperiment",
        "CreateExperiment",
        "UpdateExperiment",
        "DeleteExperiment",
        "GetRevision",
    ],
    "Realize": [
        "GetLeases",
        "GetLease",
        "CreateLease",
        "UpdateLease",
        "DeleteLease",
        "GetResources",
    ],
    "Activate": [
        "GetActivations",
        "GetActivation",
        "GetActivationStatus",
        "Activate",
        "Deactivate",
        "RebootActivation",
        "CreateActivationIngress",
        "DeleteActivationIngress",
    ],
    "Identity": [
        "ListIdentities",
        "GetIdentity",
        "Register",
        "Unregister",
        "Login",
        "Logout",
    ],
    "Cred": [
        "GetUserSSHKeys",
        "GetUserSSHCert",
    ],
    "XDC": [
        "ListXDCs",
        "GetXDC",
        "CreateXDC",
        "DeleteXDC",
        "AttachXDC",
        "DetachXDC",
        "GetXDCJumpHosts",
    ],
    "Model": [
        "Compile",
        "Push",
    ]
}

header="""\
syntax = "proto3";
package sphere.v1;

option go_package = "gitlab.com/mergetb/testbed/sphere/api/sphere/v1/go;sphere";

// MergeTB imports
import "portal/v1/alloc_types.proto";
import "portal/v1/commission_types.proto";
import "portal/v1/cred_types.proto";
import "portal/v1/identity_types.proto";
import "portal/v1/materialize_types.proto";
import "portal/v1/model_types.proto";
import "portal/v1/realize_types.proto";
import "portal/v1/workspace_types.proto";
import "portal/v1/xdc_types.proto";
import "portal/v1/wg_types.proto";

"""
print(header)

for service in methods:
    print("// %s -------------------------------------------------------------------" % service)
    print()

    for method in methods[service]:
        block = """\
message %sRequest {
    portal.v1.%sRequest request = 1;
}
message %sResponse {
    portal.v1.%sResponse response = 1;
}
""" % (method, method, method, method)
        print(block)

